<?php
/**
 * ShortcodeTest class.
 */

namespace CivicrmFrontoffice\Test;

use CivicrmApi\Api;
use CivicrmFrontoffice\Shortcode;
use PHPUnit\Framework\TestCase;
use civicrm_api3;
use stdClass;
use WP_Mock;

/**
 * Tests for the Shortcode class.
 */
class ShortcodeTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->civicrmApi->result = new stdClass();
        $this->civicrmApi->result->id = 42;

        $this->civicrmApi->values = [];

        $api = new Api($this->civicrmApi);
        Api::setInstance($api);

        $this->shortcode = new Shortcode();
    }

    /**
     * Test the setupLocales() function.
     *
     * @return void
     */
    public function testSetupLocales()
    {
        $this->assertNull($this->shortcode->setupLocales());
    }

    /**
     * Test the addQueryVars() function.
     *
     * @return void
     */
    public function testAddQueryVars()
    {
        $this->assertEquals(
            [
                'foo',
                'civicrm_id', 'civicrm_search', 'civicrm_offset', 'civicrm_limit',
                'civicrm_action', 'civicrm_add', 'civicrm_from'
            ],
            $this->shortcode->addQueryVars(['foo'])
        );
    }

    /**
     * Test the addScripts() function.
     *
     * @return void
     */
    public function testAddScripts()
    {
        $this->assertNull($this->shortcode->addScripts());
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcode()
    {
        $this->assertEquals(
            '<div class="clear">Could not find this action.</div>',
            $this->shortcode->doShortcode([], '', '')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditMember()
    {
        $this->assertContains(
            '<form method="POST"',
            $this->shortcode->doShortcode([], '', 'edit_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditMemberWithError()
    {
        $this->assertEquals(
            '<div class="clear">You are not allowed to edit this profile.</div>',
            $this->shortcode->doShortcode(['id' => 42], '', 'edit_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditMemberWithId()
    {
        $this->civicrmApi->result->contact_id = 42;

        $this->assertContains(
            '<form method="POST"',
            $this->shortcode->doShortcode(['id' => 42], '', 'edit_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditMemberWithIdAndPost()
    {
        $this->civicrmApi->result->contact_id = 42;

        $_POST = [
            'first_name' => 'first_name',
            'phone' => 'phone',
            'email' => 'email',
            '_wpnonce' => '_wpnonce'
        ];

        $this->assertContains(
            '<form method="POST"',
            $this->shortcode->doShortcode(['id' => 42], '', 'edit_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditMemberWithIdAndPostAndDo()
    {
        $this->civicrmApi->result->contact_id = 42;

        $_POST = [
            'first_name' => 'first_name',
            'do_phone' => 'do_phone',
            'do_email' => 'do_email',
            'phone' => 'phone',
            'email' => 'email',
            '_wpnonce' => '_wpnonce'
        ];

        $this->assertContains(
            '<form method="POST"',
            $this->shortcode->doShortcode(['id' => 42], '', 'edit_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditOrganization()
    {
        $this->assertEquals(
            '<div class="clear">Can\'t find this organization</div>',
            $this->shortcode->doShortcode([], '', 'edit_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditOrganizationWithId()
    {
        $this->assertEquals(
            '<div class="clear">You are not allowed to edit this organization.</div>',
            $this->shortcode->doShortcode(['id' => 42], '', 'edit_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditOrganizationWithIdAndCanEdit()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42, 'value' => 'website_type_name', 'key' => 42]
        ];

        $this->civicrmApi->result->is_permission_a_b = true;

        $this->assertContains(
            '<form method="post"',
            $this->shortcode->doShortcode(
                ['id' => 42, 'tagset' => 'tagset', 'country' => 'fr', 'note' => 'note'],
                '',
                'edit_organization'
            )
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditOrganizationWithIdAndCanEditAndPost()
    {
        $_POST = [
            'street_address'         => 'street_address',
            'supplemental_address_1' => 'street_address',
            'city'                   => 'city',
            'postal_code'            => 'postal_code',
            'email'                  => 'email',
            'phone'                  => 'phone',
            'province'               => 'province',
            'note'                   => 'note',
            'members'                => [
                42 => [
                    'role' => 'role'
                ]
            ],
            '_wpnonce' => '_wpnonce'
        ];

        $this->civicrmApi->values = [
            (object) ['id' => 42, 'value' => 'website_type_name', 'key' => 42]
        ];

        $this->civicrmApi->result->is_permission_a_b = true;

        $this->assertContains(
            '<form method="post"',
            $this->shortcode->doShortcode(
                ['id' => 42, 'country' => 'fr', 'tagset' => 'tagset', 'note' => 'note'],
                '',
                'edit_organization'
            )
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithShowOrganization()
    {
        $this->assertEquals(
            '<div class="clear">Can\'t find this organization</div>',
            $this->shortcode->doShortcode([], '', 'show_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithShowOrganizationWithId()
    {
        $this->assertContains(
            '<h2 class="contact__name">',
            $this->shortcode->doShortcode(
                ['id' => 42, 'tagset' => 'tagset', 'note' => 'note'],
                '',
                'show_organization'
            )
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithSearchOrganization()
    {
        $this->civicrmApi->lastResult = 2;

        WP_Mock::wpFunction(
            'get_query_var',
            [
                'args' => 'civicrm_search',
                'return' => [
                    'name' => 'name',
                    'province' => 'province',
                    'tag' => 'tag',
                ]
            ]
        );

        $this->civicrmApi->values = [
            (object) ['id' => 42, 'key' => 42]
        ];

        $this->civicrmApi->result->name = 'tag_name';
        $this->civicrmApi->result->tag_id = 42;
        $this->civicrmApi->result->geo_code_1 = 'geo_code_1';

        $this->assertContains(
            'strong>Found 2 organization</strong>',
            $this->shortcode->doShortcode([], '', 'search_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithSearchOrganizationWithRedirectAndTagset()
    {
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_search']);

        $this->assertContains(
            '<form action="redirect"',
            $this->shortcode->doShortcode(
                ['redirect' => 'redirect', 'tagset' => 'invalid_tagset'],
                '',
                'search_organization'
            )
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithSearchOrganizationWithCountry()
    {
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_search']);

        $this->assertContains(
            '<select name="civicrm_search[province]">',
            $this->shortcode->doShortcode(['country' => 'fr'], '', 'search_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithShowMembers()
    {
        $this->assertEquals(
            '<div class="clear">Can\'t find this organization</div>',
            $this->shortcode->doShortcode([], '', 'show_members')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithShowMembersWithId()
    {
        $this->assertEquals(
            '<div class="clear">You are not allowed to display members of this organization.</div>',
            $this->shortcode->doShortcode(['id' => 42], '', 'show_members')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithShowMembersWithIdAndIsMember()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];

        $this->assertContains(
            '<ul class="members',
            $this->shortcode->doShortcode(['id' => 42], '', 'show_members')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithShowMember()
    {
        $this->assertEquals(
            '<div class="clear">Can\'t find this member</div>',
            $this->shortcode->doShortcode([], '', 'show_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     * @todo Find a cleaner way to replace the existing get_query_var mock.
     */
    public function testDoShortcodeWithShowMemberWithFrom()
    {
        WP_Mock::tearDown();
        WP_Mock::bootstrap();
        WP_Mock::wpFunction('wp_get_current_user', ['return' => $this->user]);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_action']);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_id']);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_from', 'return' => 42]);

        $this->assertEquals(
            '<div class="clear">Can\'t find this member</div>',
            $this->shortcode->doShortcode([], '', 'show_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithShowMemberWithId()
    {
        $this->civicrmApi->result->contact_type = 'Individual';

        $this->assertContains(
            '<h3 class="contact__name">',
            $this->shortcode->doShortcode(['id' => 42], '', 'show_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithAddOrganization()
    {
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_search', 'return' => ['name' => 'name']]);

        $this->assertContains(
            '<h2>Add an organization</h2>',
            $this->shortcode->doShortcode([], '', 'add_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     * @todo Find a cleaner way to replace the existing get_query_var mock.
     */
    public function testDoShortcodeWithAddOrganizationWithId()
    {
        WP_Mock::tearDown();
        WP_Mock::bootstrap();
        WP_Mock::wpFunction('wp_get_current_user', ['return' => $this->user]);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_action']);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_id', 'return' => 42]);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_search']);
        WP_Mock::wpFunction('wp_verify_nonce', ['return' => true]);

        $_GET['_wpnonce'] = '_wpnonce';

        $this->assertContains(
            '<h2>Add an organization</h2>',
            $this->shortcode->doShortcode([], '', 'add_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     * @todo Find a cleaner way to replace the existing get_query_var mock.
     */
    public function testDoShortcodeWithRemoveOrganization()
    {
        WP_Mock::tearDown();
        WP_Mock::bootstrap();
        WP_Mock::wpFunction('wp_get_current_user', ['return' => $this->user]);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_action']);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_id', 'return' => 42]);
        WP_Mock::wpFunction('wp_verify_nonce', ['return' => true]);

        $_GET['_wpnonce'] = 'nonce';

        $this->civicrmApi->result->contact_id_a =
            $this->civicrmApi->result->contact_id = 42;

        $this->assertNull(
            $this->shortcode->doShortcode([], '', 'remove_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithRemoveOrganizationWithError()
    {
        $this->civicrmApi->result->contact_id = 42;

        $this->assertEquals(
            '<div class="clear">Can\'t find this relationship</div>',
            $this->shortcode->doShortcode([], '', 'remove_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithRemoveOrganizationWithWrongId()
    {
        WP_Mock::tearDown();
        WP_Mock::bootstrap();
        WP_Mock::wpFunction('wp_get_current_user', ['return' => $this->user]);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_action']);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_id', 'return' => 42]);

        $this->civicrmApi->result->contact_id = 42;

        $_GET['_wpnonce'] = 'nonce';

        $this->assertEquals(
            '<div class="clear">You are not allowed to edit this relationship.</div>',
            $this->shortcode->doShortcode([], '', 'remove_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithCreateOrganization()
    {
        $this->assertContains(
            '<h2>Create an organization</h2>',
            $this->shortcode->doShortcode([], '', 'create_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithCreateOrganizationWithPost()
    {
        $_POST = [
            'organization_name'      => 'organization_name',
            'street_address'         => 'street_address',
            'supplemental_address_1' => 'supplemental_address_1',
            'city'                   => 'city',
            'postal_code'            => 'postal_code',
            'email'                  => 'email',
            'phone'                  => 'phone',
            '_wpnonce'               => '_wpnonce'
        ];

        $this->assertContains(
            '<h2>Create an organization</h2>',
            $this->shortcode->doShortcode([], '', 'create_organization')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithAddMember()
    {
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_search']);

        $this->assertEquals(
            '<div class="clear">Can\'t find this organization</div>',
            $this->shortcode->doShortcode([], '', 'add_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithAddMemberWithId()
    {
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_search']);

        $this->assertEquals(
            '<div class="clear">You are not allowed to add members to this organization.</div>',
            $this->shortcode->doShortcode(['id' => 42], '', 'add_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithAddMemberWithIdAndCanEdit()
    {
        WP_Mock::wpFunction(
            'get_query_var',
            [
                'args' => 'civicrm_search',
                'return' => [
                    'first_name' => 'first_name',
                    'last_name'  => 'last_name'
                ]
            ]
        );

        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];

        $this->civicrmApi->result->is_permission_a_b = true;

        $_POST = [
            'new_member_id' => 'new_member_id',
            'new_member_role' => 'new_member_role',
            '_wpnonce' => '_wpnonce'
        ];

        $this->assertContains(
            '<h3>Search for a member</h3>',
            $this->shortcode->doShortcode(['id' => 42], '', 'add_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditRelationship()
    {
        $this->civicrmApi->result->contact_id = 42;

        $this->assertEquals(
            '<div class="clear">You are not allowed to edit this relationship.</div>',
            $this->shortcode->doShortcode([], '', 'edit_relationship')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditRelationshipWithId()
    {
        $this->civicrmApi->result->contact_id_a = 42;

        $this->assertContains(
            '<form method="post">',
            $this->shortcode->doShortcode([], '', 'edit_relationship')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     * @todo Find a cleaner way to replace the existing get_query_var mock.
     */
    public function testDoShortcodeWithEditRelationshipWithIdAndPost()
    {
        WP_Mock::tearDown();
        WP_Mock::bootstrap();
        WP_Mock::wpFunction('wp_get_current_user', ['return' => $this->user]);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_action']);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_id', 'return' => 42]);
        WP_Mock::wpFunction('wp_verify_nonce', ['return' => true]);

        $this->civicrmApi->result->contact_id =
            $this->civicrmApi->result->contact_id_a = 42;

        $_POST = [
            'new_member_role' => 'new_member_role',
            '_wpnonce' => '_wpnonce'
        ];

        $this->assertContains(
            '<form method="post">',
            $this->shortcode->doShortcode([], '', 'edit_relationship')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithEditAvatar()
    {
        $this->assertContains(
            '<h2>',
            $this->shortcode->doShortcode([], '', 'edit_avatar')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithDeleteMember()
    {
        $this->assertContains(
            '<form method="post"',
            $this->shortcode->doShortcode([], '', 'delete_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithDeleteMemberWithError()
    {
        $this->assertEquals(
            '<div class="clear">You are not allowed to edit this profile.</div>',
            $this->shortcode->doShortcode(['id' => 42], '', 'delete_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithDeleteMemberWithId()
    {
        $this->civicrmApi->result->contact_id = 42;

        $this->assertContains(
            '<form method="post"',
            $this->shortcode->doShortcode(['id' => 42], '', 'delete_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithDeleteMemberWithIdAndPost()
    {
        $this->civicrmApi->result->contact_id = 42;

        $_POST = [
            'confirm' => 'confirm',
            '_wpnonce' => '_wpnonce'
        ];

        $this->assertContains(
            '<form method="post"',
            $this->shortcode->doShortcode(['id' => 42], '', 'delete_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     */
    public function testDoShortcodeWithTwigError()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42, 'value' => 'website_type_name', 'key' => 'invalid_key']
        ];

        $this->civicrmApi->result->is_permission_a_b = true;

        $this->assertContains(
            'An exception has been thrown during the rendering of a template',
            $this->shortcode->doShortcode(['id' => 42], '', 'edit_organization')
        );
    }


    /**
     * Test the doShortcode() function.
     *
     * @return void
     * @todo Find a cleaner way to replace the existing get_query_var mock.
     */
    public function testDoShortcodeWithIdInQueryVar()
    {
        WP_Mock::tearDown();
        WP_Mock::bootstrap();
        WP_Mock::wpFunction('wp_get_current_user', ['return' => $this->user]);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_action']);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_id', 'return' => 42]);

        $this->civicrmApi->result->contact_id = 42;

        $this->assertContains(
            '<form method="POST"',
            $this->shortcode->doShortcode([], '', 'edit_member')
        );
    }

    /**
     * Test the doShortcode() function.
     *
     * @return void
     * @todo Find a cleaner way to replace the existing get_query_var mock.
     */
    public function testDoShortcodeWithActionInQueryVar()
    {
        WP_Mock::tearDown();
        WP_Mock::bootstrap();
        WP_Mock::wpFunction('wp_get_current_user', ['return' => $this->user]);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_action', 'return' => 'edit_member']);
        WP_Mock::wpFunction('get_query_var', ['args' => 'civicrm_id']);

        $this->assertContains(
            '<form method="POST"',
            $this->shortcode->doShortcode([], '', '')
        );
    }
}
