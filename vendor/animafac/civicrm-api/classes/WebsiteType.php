<?php
/**
 * Contact class.
 */

namespace CivicrmApi;

use Exception;

/**
 * Manage website types.
 *
 * The CiviCRM API does not have a WebsiteType endpoint so we use a custom object.
 */
class WebsiteType
{

    /**
     * Website type ID
     * @var int
     */
    public $id;

    /**
     * Website type name
     * @var string
     */
    public $name;

    /**
     * WebsiteType constructor.
     *
     * @param string $name Type name
     */
    public function __construct($name)
    {
        $api = Api::getInstance();
        foreach ($api->getOptions('Website', 'website_type_id') as $type) {
            if ($type->value == $name) {
                $this->id = $type->key;
                $this->name = $type->value;
                return;
            }
        }

        throw new Exception("Can't find this website type: ".$name);
    }

    /**
     * Get all available website types.
     * @return self[]
     */
    public static function getAll()
    {
        $api = Api::getInstance();
        $return = [];
        foreach ($api->getOptions('Website', 'website_type_id') as $type) {
            $return[] = new self($type->value);
        }

        return $return;
    }
}
