<?php
/**
 * ContactTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Tag;

use stdClass;

/**
 * Tests for the Contact class.
 */
class TagTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->civicrmApi->result = (object) [
            'name' => 'tag_name',
            'id' => 42
        ];

        $this->tag = new Tag(42);
        $this->civicrmApi->result = new stdClass();
    }

    /**
     * Test the get() function.
     *
     * @return void
     */
    public function testGet()
    {
        $this->assertInternalType('int', $this->tag->get('id'));
        $this->assertEquals('tag_name', $this->tag->get('name'));
    }

    /**
     * Test the getAll() function.
     *
     * @return void
     */
    public function testGetAll()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        foreach (Tag::getAll() as $tag) {
            $this->assertInstanceOf(Tag::class, $tag);
        }
    }

    /**
     * Test the getAllTagsets() function.
     *
     * @return void
     */
    public function testGetAllTagsets()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        foreach (Tag::getAllTagsets() as $tag) {
            $this->assertInstanceOf(Tag::class, $tag);
        }
    }

    /**
     * Test the getChildren() function.
     *
     * @return void
     */
    public function testGetChildren()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        foreach ($this->tag->getChildren() as $tag) {
            $this->assertInstanceOf(Tag::class, $tag);
        }
    }

    /**
     * Test the __toString() function.
     *
     * @return void
     */
    public function testToString()
    {
        $this->assertEquals('tag_name', $this->tag->__toString());
        $this->assertEquals('tag_name', (string) $this->tag);
    }
}
