<?php
/**
 * WebsiteTypeTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\WebsiteType;

/**
 * Tests for the WebsiteType class.
 */
class WebsiteTypeTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();
    }

    /**
     * Test the getAll() function.
     *
     * @return void
     */
    public function testGetAll()
    {
        $this->civicrmApi->values = [
            (object) [
                'value' => 'website_type_name',
                'key' => 42
            ]
        ];
        foreach (WebsiteType::getAll() as $websiteType) {
            $this->assertInstanceOf(WebsiteType::class, $websiteType);
        }
    }
}
