<?php
/**
 * StateProvinceTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Country;
use CivicrmApi\StateProvince;

/**
 * Tests for the StateProvince class.
 */
class StateProvinceTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->civicrmApi->result = (object) [
            'id' => 42
        ];
        $this->stateProvince = new StateProvince(42);
    }

    /**
     * Test the getAllFromCountry() function.
     *
     * @return void
     */
    public function testGetAllFromCountry()
    {
        $this->civicrmApi->values = [
            (object) ['id' => 42]
        ];
        foreach (StateProvince::getAllFromCountry(new Country(42)) as $stateProvince) {
            $this->assertInstanceOf(StateProvince::class, $stateProvince);
        }
    }
}
