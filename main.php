<?php
/*
Plugin Name: CiviCRM Front Office
Version: 1.0.2
Author: Animafac
Author URI: https://www.animafac.net/
Plugin URI: https://framagit.org/Animafac/wp-civicrm-frontoffice
Description: Add shortcodes that allow users to edit their own organization in CiviCRM
 */

/**
 * Main plugin entry point.
 */

use CivicrmApi\Api;
use CivicrmFrontoffice\Shortcode;

require_once __DIR__.'/vendor/autoload.php';
require_once ABSPATH.'/wp-admin/includes/user.php';

if (defined('WP_PLUGIN_DIR')) {
    require_once WP_PLUGIN_DIR.'/civicrm/civicrm/api/class.api.php';

    $uploadDir = wp_upload_dir();
    Api::$path = $uploadDir['basedir'].'/civicrm/';

    $shortcode = new Shortcode();

    add_shortcode('edit_member', [$shortcode, 'doShortcode']);
    add_shortcode('edit_organization', [$shortcode, 'doShortcode']);
    add_shortcode('show_organization', [$shortcode, 'doShortcode']);
    add_shortcode('search_organization', [$shortcode, 'doShortcode']);
    add_shortcode('show_members', [$shortcode, 'doShortcode']);
    add_shortcode('show_member', [$shortcode, 'doShortcode']);
    add_shortcode('add_organization', [$shortcode, 'doShortcode']);
    // We don't have a remove_organization shortcode.
    add_shortcode('create_organization', [$shortcode, 'doShortcode']);
    add_shortcode('add_member', [$shortcode, 'doShortcode']);
    add_shortcode('edit_relationship', [$shortcode, 'doShortcode']);
    add_shortcode('edit_avatar', [$shortcode, 'doShortcode']);
    add_shortcode('delete_member', [$shortcode, 'doShortcode']);

    add_filter('query_vars', [$shortcode, 'addQueryVars']);
    add_action('plugins_loaded', [$shortcode, 'setupLocales']);
    add_action('wp_enqueue_scripts', [$shortcode, 'addScripts']);
}
