/**
 * Gettext Scanner Script for Twig Projects
 * v1.2
 *
 * Developed by Luís Silva
 * https://github.com/luism-s
 */

/**
 * Description:
 * While developing templates using the Twig Template Engine, one might find easier to use gettext
 * functions in .twig files for string translation, or wordpress translate functions if it's the case
 * (Ex: Timber WP Plugin). To simplify the scanning of .twig files for those same functions, this
 * script was built to parse .twig files, wrap occurrences of gettext function calls in php tags and
 * output the result as a .php file so it can be parsed normally by POEdit.
 *
 * Usage:
 * `gulp pot`
 *
 * Logic:
 * - Iterates over all given .twig files
 * - Search and replace for gettext functions in Twig files and wraps them around PHP tags
 * - Outputs each file as .php into a cache folder
 * - Scan all .php files for gettext functions using 'gulp-wp-pot' (cache included)
 * - Generate .pot file
 *
 * Dependencies:
 * `npm install gulp gulp-if del gulp-wp-pot gulp-replace gulp-rename run-sequence`
 *
 * Warning:
 * This script has only ben tested in the context of Wordpress theme development using Timber.
 *
 * TODO:
 * Cover `translate_nooped_plural` function.
 */
var gulp        = require('gulp');
var gulpif      = require('gulp-if');
var del         = require('del');
var wpPot       = require('gulp-wp-pot');
var replace     = require('gulp-replace');
var rename      = require('gulp-rename');
var runSequence = require('run-sequence');

/**
 * Configuration Options
 *
 * All paths are as if this script is
 * located in the root of the theme and all the Twig
 * files are located under /views
 */
var config = {
  "text_domain" : "civicrm-frontoffice",       // Replace with your domain
  "twig_files"  : "templates/**/*.twig",  // Twig Files
  "php_files"   : ["cache/**/*.php", "classes/*.php"],         // PHP Files
  "cacheFolder" : "cache",      // Cache Folder
  "destFolder"  : "languages",        // Folder where .pot file will be saved
  "keepCache"   : true                // Delete cache files after script finishes
};

/**
 * Main Task
 */
gulp.task('pot', function(callback) {
  runSequence('compile-twigs', 'generate-pot', callback);
});

/**
 * Generate POT file from all .php files in the theme,
 * including the cache folder.
 */
gulp.task('generate-pot', function() {
  return gulp.src(config.php_files)
    .pipe(wpPot( {
      domain: config.text_domain
    } ))
    .pipe(gulp.dest(config.destFolder + '/' + config.text_domain + '.pot'))
    .pipe(gulpif(!config.keepCache, del.bind(null, [config.cacheFolder], {force: true})));
});

/**
 * Fake Twig Gettext Compiler
 *
 * Searches and replaces all occurences of __('string', 'domain'), _e('string', 'domain') and so on,
 * with <?php __('string', 'domain'); ?> or <?php _e('string', 'domain'); ?> and saves the content
 * in a .php file with the same name in the cache folder.
 *
 * Functions supported:
 *
 * Simple: __(), _e(), translate()
 * Plural: _n()
 * Disambiguation: _x(), _ex(), _nx()
 * Noop: _n_loop(), _nx_noop()
 *
 * TODO:
 * - Support translate_nooped_plural() function
 * - Skip gettext calls insied Twig comments (ex. {# __('string', 'domain') #})
 */
gulp.task('compile-twigs', function() {
  del.bind(null, [config.cacheFolder], {force: true})

  /**
   * __
   * _e
   * _x
   * _xn
   * _ex
   * _n_noop
   * _nx_noop
   * translate  -> Match __,  _e, _x and so on
   * \(         -> Match (
   * \s*?       -> Match empty space 0 or infinite times, as few times as possible (ungreedy)
   * [\'\"]     -> Match ' or "
   * .+?        -> Match any character, 1 to infinite times, as few times as possible (ungreedy)
   * ,          -> Match ,
   * .+?        -> Match any character, 1 to infinite times, as few times as possible (ungreedy)
   * \)         -> Match )
   */
  var gettext_regex = {

    // _e( "text", "domain" )
    // __( "text", "domain" )
    // translate( "text", "domain" )
    // esc_attr__( "text", "domain" )
    // esc_attr_e( "text", "domain" )
    // esc_html__( "text", "domain" )
    // esc_html_e( "text", "domain" )
    simple : /(__|_e|translate|esc_attr__|esc_attr_e|esc_html__|esc_html_e)\(\s*?[\'\"].+?[\'\"]\s*?,\s*?[\'\"].+?[\'\"]\s*?\)/g,

    // _n( "single", "plural", number, "domain" )
    plural : /_n\(\s*?[\'\"].*?[\'\"]\s*?,\s*?[\'\"].*?[\'\"]\s*?,\s*?.+?\s*?,\s*?[\'\"].+?[\'\"]\s*?\)/g,

    // _x( "text", "context", "domain" )
    // _ex( "text", "context", "domain" )
    // esc_attr_x( "text", "context", "domain" )
    // esc_html_x( "text", "context", "domain" )
    // _nx( "single", "plural", "number", "context", "domain" )
    disambiguation : /(_x|_ex|_nx|esc_attr_x|esc_html_x)\(\s*?[\'\"].+?[\'\"]\s*?,\s*?[\'\"].+?[\'\"]\s*?,\s*?[\'\"].+?[\'\"]\s*?\)/g,

    // _n_noop( "singular", "plural", "domain" )
    // _nx_noop( "singular", "plural", "context", "domain" )
    noop : /(_n_noop|_nx_noop)\((\s*?[\'\"].+?[\'\"]\s*?),(\s*?[\'\"]\w+?[\'\"]\s*?,){0,1}\s*?[\'\"].+?[\'\"]\s*?\)/g
  };

  // Iterate over .twig files
  gulp.src(config.twig_files)

    // Search for Gettext function calls and wrap them around PHP tags.
    .pipe(replace(gettext_regex.simple, function( match ){
      return '<?php ' + match + '; ?>';
    }))
    .pipe(replace(gettext_regex.plural, function( match ){
      return '<?php ' + match + '; ?>';
    }))
    .pipe(replace(gettext_regex.disambiguation, function( match ){
      return '<?php ' + match + '; ?>';
    }))
    .pipe(replace(gettext_regex.noop, function( match ){
      return '<?php ' + match + '; ?>';
    }))

    // Rename file with .php extension
    .pipe(rename(function( file_path ) {
      file_path.extname = ".php"
    }))

    // Output the result to the cache folder as a .php file.
    .pipe(gulp.dest(config.cacheFolder));
});
