<?php
/**
 * Shortcode class.
 */

namespace CivicrmFrontoffice;

use CivicrmApi\ApiException;
use CivicrmApi\Contact;
use CivicrmApi\Country;
use CivicrmApi\Gender;
use CivicrmApi\LocationType;
use CivicrmApi\Note;
use CivicrmApi\Relationship;
use CivicrmApi\RelationshipType;
use CivicrmApi\StateProvince;
use CivicrmApi\Tag;
use CivicrmApi\WebsiteType;

use Twig_Environment;
use Twig_Error_Runtime;
use Twig_SimpleFilter;
use Twig_SimpleFunction;
use Twig_Loader_Filesystem;

/**
 * Generate various WordPress shortcodes.
 */
class Shortcode
{

    /**
     * Twig instance
     * @var Twig_Environment
     */
    private $twig;

    /**
     * Shortcode constructor.
     */
    public function __construct()
    {
        $loader = new Twig_Loader_Filesystem(__DIR__.'/../templates/');
        $this->twig = new Twig_Environment($loader);

        // Add some useful WordPress functions.
        $this->twig->addFunction(new Twig_SimpleFunction('__', '__'));
        $this->twig->addFunction(new Twig_SimpleFunction('_n', '_n'));
        $this->twig->addFunction(new Twig_SimpleFunction('add_query_arg', 'add_query_arg'));
        $this->twig->addFunction(new Twig_SimpleFunction('get_query_var', 'get_query_var'));
        $this->twig->addFunction(new Twig_SimpleFunction('home_url', 'home_url'));
        $this->twig->addFunction(new Twig_SimpleFunction('plugin_dir_url', [$this, 'getPluginUrl']));
        $this->twig->addFunction(new Twig_SimpleFunction('shortcode_exists', 'shortcode_exists'));
        $this->twig->addFunction(new Twig_SimpleFunction('wp_create_nonce', 'wp_create_nonce'));
        $this->twig->addFunction(
            new Twig_SimpleFunction('wp_nonce_field', [$this, 'getNonceField'], ['is_safe' => ['html']])
        );
        $this->twig->addFilter(new Twig_SimpleFilter('stripslashes', 'stripslashes'));

        // Parse shortcodes.
        $this->twig->addFilter(new Twig_SimpleFilter('shortcodes', 'do_shortcode', ['is_safe' => ['html']]));
    }

    /**
     * Add an hidden nonce input
     * @param  string $action Context of the nonce
     * @return string HTML input
     */
    public function getNonceField($action)
    {
        return wp_nonce_field($action, '_wpnonce', true, false);
    }

    /**
     * Get plugin folder absolute URL
     * @return string URL
     */
    public function getPluginUrl()
    {
        return plugin_dir_url(__DIR__);
    }

    /**
     * Setup WordPress locales.
     * @return void
     */
    public function setupLocales()
    {
        load_plugin_textdomain('civicrm-frontoffice', false, dirname(plugin_basename(__DIR__)).'/languages');
    }

    /**
     * Register new query variables in WordPress.
     *
     * @param string[] $vars Variables to add.
     */
    public function addQueryVars(array $vars)
    {
        $vars[] = 'civicrm_id';
        $vars[] = 'civicrm_search';
        $vars[] = 'civicrm_offset';
        $vars[] = 'civicrm_limit';
        $vars[] = 'civicrm_action';
        $vars[] = 'civicrm_add';
        $vars[] = 'civicrm_from';
        return $vars;
    }

    /**
     * Load JavaScript and CSS.
     */
    public function addScripts()
    {
        wp_enqueue_style('leaflet', plugins_url('node_modules/leaflet/dist/leaflet.css', __DIR__), [], '1.3');
        wp_enqueue_script('leaflet', plugins_url('node_modules/leaflet/dist/leaflet.js', __DIR__), [], '1.3');
        wp_enqueue_script('civicrm-frontoffice-map', plugins_url('js/map.js', __DIR__), ['jquery', 'leaflet']);
    }

    /**
     * Get the current user.
     *
     * @return Contact User
     */
    private function getUser()
    {
        $user = wp_get_current_user();
        if ($user->ID > 0) {
            return Contact::getByUserId($user->ID);
        }
    }

    /**
     * Get the current contact.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return Contact
     */
    private function getContact($attributes, $fallback = false)
    {
        if (!empty(get_query_var('civicrm_id'))) {
            // We try to get the contact ID from a get parameter first.
            $id = (int) get_query_var('civicrm_id');
        } elseif (is_array($attributes) && isset($attributes['id'])) {
            // And then fall back on the shortcode attribute.
            $id = $attributes['id'];
        }

        if (isset($id) && $id > 0) {
            return new Contact($id);
        } elseif ($fallback) {
            return $this->getUser();
        }
    }

    /**
     * Create the Twig context for the organization create/edit form.
     *
     * @param array        $context    Twig contact
     * @param array|string $attributes Shortcode attributes
     *
     * @return array Context
     */
    private function addOrganizationFormContext(array $context, $attributes)
    {
        $context['websiteTypes'] = WebsiteType::getAll();

        if (is_array($attributes) && isset($attributes['tagset'])) {
            $context['tagset'] = Tag::getSingle(['name' => $attributes['tagset']]);
        }

        if (is_array($attributes) && isset($attributes['country'])) {
            $context['provinces'] = StateProvince::getAllFromCountry(
                Country::getSingle(['iso_code' => $attributes['country']])
            );
        } else {
            $context['provinces'] = StateProvince::getAll();
        }

        if (is_array($attributes) && isset($attributes['note'])) {
            $context['noteName'] = $attributes['note'];
            if (isset($context['contact'])) {
                try {
                    $context['note'] = Note::getSingle(
                        [
                            'entity_table' => 'civicrm_contact',
                            'entity_id' => $context['contact']->get('contact_id'),
                            'subject' => $attributes['note']
                        ]
                    );
                } catch (ApiException $e) {
                    // The organization does not have any note.
                }
            }
        }

        return $context;
    }

    /**
     * Apply information entered in form to Contact object.
     *
     * @param Contact      $organization Organization
     * @param array|string $attributes   Shortcode attributes
     *
     * @return Contact Organization
     */
    private function applyOrganizationForm(Contact $organization, $attributes)
    {
        foreach (['organization_name', 'nick_name'] as $field) {
            if (isset($_POST[$field]) && !empty($_POST[$field])) {
                $organization->set($field, stripslashes($_POST[$field]));
            }
        }

        $locationType = LocationType::getDefaultType();

        if (is_array($attributes) && isset($attributes['country'])) {
            $country = Country::getSingle(['iso_code' => $attributes['country']]);
        } else {
            $country = null;
        }
        if (!empty($_POST['province'])) {
            $province = new StateProvince(stripslashes($_POST['province']));
        } else {
            $province = null;
        }

        $organization->setAddress(
            stripslashes($_POST['street_address']),
            stripslashes($_POST['supplemental_address_1']),
            stripslashes($_POST['city']),
            stripslashes($_POST['postal_code']),
            $locationType,
            $country,
            $province
        );

        $organization->setEmail(stripslashes($_POST['email']), $locationType);
        $organization->setPhone(stripslashes($_POST['phone']), $locationType);

        foreach (WebsiteType::getAll() as $type) {
            if (isset($_POST['websites'][$type->id])) {
                $organization->setWebsite($type, $_POST['websites'][$type->id]);
            }
        }

        if (is_array($attributes) && isset($attributes['tagset'])) {
            $tagset = Tag::getSingle(['name' => $attributes['tagset']]);
            foreach ($tagset->getChildren() as $tag) {
                if (isset($_POST['tags'][$tag->get('id')])) {
                    $organization->addTag($tag);
                } else {
                    $organization->removeTag($tag);
                }
            }
        }

        if (is_array($attributes) && isset($attributes['note'])) {
            try {
                $note = Note::getSingle(
                    [
                        'entity_table' => 'civicrm_contact',
                        'entity_id' => $organization->get('contact_id'),
                        'subject' => $attributes['note']
                    ]
                );
            } catch (ApiException $e) {
                $note = new Note();
                $note->set('entity_table', 'civicrm_contact');
                $note->set('entity_id', $organization->get('contact_id'));
                $note->set('subject', $attributes['note']);
            }

            // Don't try to create an empty note, it will fail.
            if (!empty($_POST['note']) || $note->get('id') != null) {
                $note->set('contact_id', $organization->get('contact_id'));
                $note->set('note', stripslashes($_POST['note']));
                $note->save();
            }
        }

        return $organization;
    }

    /**
     * Display a form allowing the user to edit an organization.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function editOrganization($attributes)
    {
        $context = [
            'user' => $this->getUser(),
            'contact' => $this->getContact($attributes)
        ];

        if (!isset($context['contact'])) {
            return $this->error(__("Can't find this organization", 'civicrm-frontoffice'));
        }

        if (!isset($context['user']) || !$context['user']->canEdit($context['contact'])) {
            return $this->error(__('You are not allowed to edit this organization.', 'civicrm-frontoffice'));
        }

        $context = $this->addOrganizationFormContext($context, $attributes);

        // If the user submitted the form, we edit the organization.
        if (!empty($_POST)) {
            if (!wp_verify_nonce($_POST['_wpnonce'], 'edit_organization')) {
                return $this->error(__('You are not allowed to edit this organization.', 'civicrm-frontoffice'));
            }

            $context['contact'] = $this->applyOrganizationForm($context['contact'], $attributes);

            $context['contact']->save();
            wp_redirect(add_query_arg('civicrm_action', 'show_organization'));
        }

        return $this->twig->render('edit_organization.twig', $context);
    }

    /**
     * Display information about an organization.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function showOrganization($attributes)
    {
        $context = [
            'user' => $this->getUser(),
            'contact' => $this->getContact($attributes)
        ];

        if (!isset($context['contact'])) {
            return $this->error(__("Can't find this organization", 'civicrm-frontoffice'));
        }

        if (is_array($attributes) && isset($attributes['tagset'])) {
            $context['tagset'] = Tag::getSingle(['name' => $attributes['tagset']]);
        }

        if (is_array($attributes) && isset($attributes['note'])) {
            try {
                $context['note'] = Note::getSingle(
                    [
                        'entity_table' => 'civicrm_contact',
                        'entity_id' => $context['contact']->get('contact_id'),
                        'subject' => $attributes['note']
                    ]
                );
            } catch (ApiException $e) {
                // The organization does not have any note.
            }
        }

        return $this->twig->render('show_organization.twig', $context);
    }

    /**
     * Display a form allowing the user to search for an organization.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function searchOrganization($attributes)
    {
        $context = [
            'tags' => [],
            'search' => get_query_var('civicrm_search'),
            'offset' => get_query_var('civicrm_offset', 0),
            'limit' => get_query_var('civicrm_limit', 10)
        ];

        if (is_array($attributes) && isset($attributes['redirect'])) {
            // If the form redirects to another page, this will be handled on this other page.
            $context['redirect'] = $attributes['redirect'];
        } elseif (!empty($context['search'])) {
            $constraint = [
                'contact_type' => 'Organization',
                'display_name' => stripslashes($context['search']['name']),
                'state_province_id' => $context['search']['province'],
                'do_not_email' => 0,
                'tag' => $context['search']['tag'],
                'options' => [
                    'offset' => $context['offset'],
                    'limit' => $context['limit'],
                    'sort' => 'sort_name'
                ]
            ];
            $context['contacts'] = Contact::getAll($constraint);
            $context['nbContacts'] = Contact::getCount($constraint);
            $context['maxOffset'] = (int) ($context['nbContacts'] / $context['limit']) * $context['limit'];

            // We try to display contacts with an address on a map.
            $context['map'] = [];
            foreach ($context['contacts'] as $contact) {
                if ($contact->get('geo_code_1')) {
                    $context['map'][] = [
                        'name' => $contact->get('display_name'),
                        'description' => $contact->get('street_address').'<br/>'.
                            $contact->get('postal_code').' '.$contact->get('city').'<br /><br />'.
                            '<a target="_blank" href="'.
                                add_query_arg(
                                    ['civicrm_action' => 'show_organization', 'civicrm_id' => $contact->get('id')],
                                    ''
                                ).'">
                                En savoir plus
                            </a>',
                        'coord' => [
                            'lon' => $contact->get('geo_code_2'),
                            'lat' => $contact->get('geo_code_1')
                        ]
                    ];
                }
            }
        }

        if (is_array($attributes) && isset($attributes['country'])) {
            $context['provinces'] = StateProvince::getAllFromCountry(
                Country::getSingle(['iso_code' => $attributes['country']])
            );
        } else {
            $context['provinces'] = StateProvince::getAll();
        }

        if (is_array($attributes) && isset($attributes['tagset'])) {
            try {
                $context['tagset'] = Tag::getSingle(['name' => $attributes['tagset']]);
            } catch (ApiException $e) {
                // The tagset does not exist.
            }
        }

        return $this->twig->render('search_organization.twig', $context);
    }

    /**
     * Disable a relationship between an user and an organization.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function removeOrganization($attributes)
    {
        $context = [
            'user' => $this->getUser()
        ];

        $id = get_query_var('civicrm_id');
        if (empty($id)) {
            return $this->error(__("Can't find this relationship", 'civicrm-frontoffice'));
        }

        if (!wp_verify_nonce($_GET['_wpnonce'], 'remove_organization')) {
            return $this->error(__('You are not allowed to edit this relationship.', 'civicrm-frontoffice'));
        }

        $relationship = new Relationship($id);
        $contact = new Contact($relationship->get('contact_id_a'));
        $organization = new Contact($relationship->get('contact_id_b'));

        if (isset($context['user'])
            && ($context['user']->canEdit($organization)
                || $context['user']->get('id') === $contact->get('id'))
        ) {
            $relationship->set('is_active', false);
            $relationship->save();

            // We then redirect to the member page.
            wp_redirect(
                add_query_arg(
                    [
                        'civicrm_action' => 'show_member',
                        'civicrm_id' => $relationship->get('contact_id_a')
                    ],
                    ''
                )
            );
        } else {
            return $this->error(__('You are not allowed to edit this relationship.', 'civicrm-frontoffice'));
        }
    }

    /**
     * Display a form allowing the user to add a relationship with an organization.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function addOrganization($attributes)
    {
        $context = [
            'user' => $this->getUser(),
            'search' => get_query_var('civicrm_search'),
            'relationshipType' => new RelationshipType(6)
        ];

        if (!isset($context['user'])) {
            return $this->error(__('You must be logged in to do this.', 'civicrm-frontoffice'));
        }

        // The user selected an organization to add.
        if (isset($_POST['organization_id'])) {
            if (!wp_verify_nonce($_POST['_wpnonce'], 'add_member')) {
                return $this->error(__('You are not allowed to edit this profile.', 'civicrm-frontoffice'));
            }

            $organization = new Contact($_POST['organization_id']);
            $relationship = $context['user']->addRelationship(
                $organization,
                new RelationshipType(6),
                !$organization->hasAdmin(),
                stripslashes($_POST['new_member_role'])
            );
            wp_redirect(
                add_query_arg(
                    [
                        'civicrm_action' => 'show_member',
                        'civicrm_id' => $relationship->get('contact_id_a')
                    ],
                    ''
                )
            );
        }

        // The user typed the name of an organization.
        if (!empty($context['search'])) {
            $constraint = [
                'contact_type' => 'Organization',
                'display_name' => $context['search']['name']
            ];
            $context['contacts'] = Contact::getAll($constraint);
        }

        return $this->twig->render('add_organization.twig', $context);
    }

    /**
     * Display a form allowing to edit an individual.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function editMember($attributes)
    {
        $context = [
            'user' => $this->getUser(),
            'contact' => $this->getContact($attributes, true)
        ];

        if (!isset($context['contact'])) {
            return $this->error(__("Can't find this member", 'civicrm-frontoffice'));
        }

        if (!isset($context['user']) || $context['user']->get('id') != $context['contact']->get('id')) {
            return $this->error(__('You are not allowed to edit this profile.', 'civicrm-frontoffice'));
        }

        if (!empty($_POST)) {
            if (!wp_verify_nonce($_POST['_wpnonce'], 'edit_member')) {
                return $this->error(__('You are not allowed to edit this profile.', 'civicrm-frontoffice'));
            }

            foreach (['first_name', 'last_name', 'gender_id'] as $field) {
                if (isset($_POST[$field]) && (!empty($_POST[$field]) || $field == 'gender_id')) {
                    $context['contact']->set($field, stripslashes($_POST[$field]));
                }
            }

            if (isset($_POST['do_phone']) && !empty($_POST['do_phone'])) {
                $context['contact']->set('do_not_phone', false);
            } else {
                $context['contact']->set('do_not_phone', true);
            }

            if (isset($_POST['do_email']) && !empty($_POST['do_email'])) {
                $context['contact']->set('do_not_email', false);
            } else {
                $context['contact']->set('do_not_email', true);
            }

            $locationType = LocationType::getDefaultType();

            $context['contact']->setPhone(stripslashes($_POST['phone']), $locationType);
            $context['contact']->setEmail(stripslashes($_POST['email']), $locationType);

            $context['contact']->save();

            wp_redirect(
                add_query_arg(
                    ['civicrm_action' => 'show_member', 'civicrm_id' => $context['contact']->get('id')],
                    ''
                )
            );
        }

        $context['genders'] = Gender::getAll();

        return $this->twig->render('edit_member.twig', $context);
    }

    /**
     * Display a form allowing to edit your avatar.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function editAvatar($attributes)
    {
        $context = [
            'user' => $this->getUser(),
            'contact' => $this->getContact($attributes, true)
        ];

        if (!isset($context['contact'])) {
            return $this->error(__("Can't find this member", 'civicrm-frontoffice'));
        }

        if (!isset($context['user']) || $context['user']->get('id') != $context['contact']->get('id')) {
            return $this->error(__('You are not allowed to edit this profile.', 'civicrm-frontoffice'));
        }

        // Can't use get_query_var() because "updated" is not registered.
        if (isset($_GET['updated']) && $_GET['updated']) {
            wp_redirect(
                add_query_arg(
                    [
                        'civicrm_action' => 'show_member',
                        'civicrm_id' => $context['contact']->get('id')
                    ],
                    ''
                )
            );
        }

        return $this->twig->render('edit_avatar.twig', $context);
    }

    /**
     * Display information about an individual.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function showMember($attributes)
    {
        $context = [
            'user' => $this->getUser(),
            'contact' => $this->getContact($attributes, true),
            'relationshipType' => new RelationshipType(6)
        ];

        if (get_query_var('civicrm_from')) {
            $context['from'] = new Contact(get_query_var('civicrm_from'));
        }

        if (!isset($context['contact']) || $context['contact']->get('contact_type') != 'Individual') {
            return $this->error(__("Can't find this member", 'civicrm-frontoffice'));
        }

        return $this->twig->render('show_member.twig', $context);
    }

    /**
     * Display a list of members in an organization.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function createOrganization($attributes)
    {
        $context = [
            'user' => $this->getUser()
        ];

        if (!isset($context['user'])) {
            return $this->error(__('You must be logged in to do this.', 'civicrm-frontoffice'));
        }

        $context = $this->addOrganizationFormContext($context, $attributes);

        // If the user submitted the form, we create the organization.
        if (!empty($_POST) && !empty($_POST['organization_name'])) {
            if (!wp_verify_nonce($_POST['_wpnonce'], 'edit_organization')) {
                return $this->error(__('You are not allowed to edit this organization.', 'civicrm-frontoffice'));
            }

            $organization = new Contact();
            $organization->set('contact_type', 'Organization');
            $organization->set('organization_name', stripslashes($_POST['organization_name']));
            $organization = new Contact($organization->save());

            $organization = $this->applyOrganizationForm($organization, $attributes);

            $organization->save();

            $context['user']->addRelationship(
                $organization,
                new RelationshipType(6),
                true
            );

            wp_redirect(
                add_query_arg(
                    [
                        'civicrm_action' => 'show_organization',
                        'civicrm_id' => $organization->get('contact_id')
                    ]
                )
            );
        }

        return $this->twig->render('create_organization.twig', $context);
    }

    /**
     * Display an error.
     *
     * @param string $message Error message
     *
     * @return string HTML
     */
    private function error($message)
    {
        return '<div class="clear">'.$message.'</div>';
    }

    /**
     * Display a form allowing to create a new organization.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function showMembers($attributes)
    {
        $context = [
            'user' => $this->getUser(),
            'contact' => $this->getContact($attributes),
            'relationshipType' => new RelationshipType(6)
        ];

        if (!isset($context['contact'])) {
            return $this->error(__("Can't find this organization", 'civicrm-frontoffice'));
        } elseif (!isset($context['user'])
            || !$context['user']->isMember($context['contact'], $context['relationshipType'])
        ) {
            return $this->error(
                __(
                    'You are not allowed to display members of this organization.',
                    'civicrm-frontoffice'
                )
            );
        }

        return $this->twig->render('show_members.twig', $context);
    }

    /**
     * Display a form allowing to add a member to an organization.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function addMember($attributes)
    {
        $context = [
            'user' => $this->getUser(),
            'contact' => $this->getContact($attributes),
            'search' => get_query_var('civicrm_search'),
            'relationshipType' => new RelationshipType(6)
        ];

        if (!isset($context['contact'])) {
            return $this->error(__("Can't find this organization", 'civicrm-frontoffice'));
        } elseif (!isset($context['user']) || !$context['user']->canEdit($context['contact'])) {
            return $this->error(__('You are not allowed to add members to this organization.', 'civicrm-frontoffice'));
        }

        if (isset($_POST['new_member_id'])) {
            if (!wp_verify_nonce($_POST['_wpnonce'], 'add_member')) {
                return $this->error(__('You are not allowed to edit this profile.', 'civicrm-frontoffice'));
            }

            $member = new Contact($_POST['new_member_id']);
            $relationship = $member->addRelationship(
                $context['contact'],
                new RelationshipType(6),
                isset($_POST['new_member_admin']),
                stripslashes($_POST['new_member_role'])
            );
            wp_redirect(
                add_query_arg(
                    [
                        'civicrm_action' => 'show_members',
                        'civicrm_id' => $relationship->get('contact_id_b')
                    ],
                    ''
                )
            );
        }

        // The user typed the name of a member.
        if (!empty($context['search'])) {
            $constraint = [
                'contact_type' => 'Individual',
                'first_name' => $context['search']['first_name'],
                'last_name' => $context['search']['last_name']
            ];
            $context['contacts'] = Contact::getAll($constraint);
        }

        return $this->twig->render('add_member.twig', $context);
    }

    /**
     * Display a form allowing to edit a relationship.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function editRelationship($attributes)
    {
        $context = [
            'user' => $this->getUser()
        ];

        $context['relationship'] = new Relationship(get_query_var('civicrm_id'));
        $context['contact'] = new Contact($context['relationship']->get('contact_id_a'));
        $context['organization'] = new Contact($context['relationship']->get('contact_id_b'));

        if (isset($context['user'])
            && ($context['user']->canEdit($context['organization'])
                || $context['user']->get('id') === $context['contact']->get('id'))
        ) {
            if (!empty($_POST)) {
                if (!wp_verify_nonce($_POST['_wpnonce'], 'edit_relationship')) {
                    return $this->error(__('You are not allowed to edit this relationship.', 'civicrm-frontoffice'));
                }

                $context['relationship']->set('description', stripslashes($_POST['new_member_role']));
                if (isset($_POST['new_member_admin'])) {
                    $permission = 1;
                } else {
                    $permission = 2;
                }
                $context['relationship']->set('is_permission_a_b', $permission);
                $context['relationship']->save();
                wp_redirect(
                    add_query_arg(
                        [
                            'civicrm_action' => 'show_member',
                            'civicrm_id'     => $context['relationship']->get('contact_id_a'),
                            'civicrm_from'   => $context['organization']->get('id')
                        ],
                        ''
                    )
                );
            }

            return $this->twig->render('edit_relationship.twig', $context);
        } else {
            return $this->error(__('You are not allowed to edit this relationship.', 'civicrm-frontoffice'));
        }
    }

    /**
     * Display a form allowing to delete an individual.
     *
     * The CiviCRM is not deleted, only the WordPress user is deleted.
     *
     * @param array|string $attributes Shortcode attributes
     *
     * @return string HTML
     */
    private function deleteMember($attributes)
    {
        $context = [
            'user' => $this->getUser(),
            'contact' => $this->getContact($attributes, true)
        ];

        if (!isset($context['contact'])) {
            return $this->error(__("Can't find this member", 'civicrm-frontoffice'));
        }

        if (!isset($context['user']) || $context['user']->get('id') != $context['contact']->get('id')) {
            return $this->error(__('You are not allowed to edit this profile.', 'civicrm-frontoffice'));
        }

        if (isset($_POST['confirm']) && $_POST['confirm']) {
            if (!wp_verify_nonce($_POST['_wpnonce'], 'delete_member')) {
                return $this->error(__('You are not allowed to edit this profile.', 'civicrm-frontoffice'));
            }

            // We remove consent before deleting the account.
            $context['contact']->set('do_not_phone', true);
            $context['contact']->set('do_not_email', true);
            $context['contact']->save();

            wp_delete_user($context['contact']->getUser()->get('id'));

            // We redirect to the home page.
            wp_redirect(home_url());
        }

        return $this->twig->render('delete_member.twig', $context);
    }

    /**
     * Display the requested shortcode.
     *
     * @param array|string $attributes Shortcode attributes
     * @param string       $content    Shortcode content
     * @param string       $name       Shortcode name
     *
     * @return string HTML
     */
    public function doShortcode($attributes, $content, $name)
    {
        // Shortcode can be overridden with a GET parameter.
        $action = get_query_var('civicrm_action');
        if (!empty($action)) {
            $name = $action;
        }

        try {
            switch ($name) {
                case 'edit_member':
                    return $this->editMember($attributes);
                case 'edit_organization':
                    return $this->editOrganization($attributes);
                case 'show_organization':
                    return $this->showOrganization($attributes);
                case 'search_organization':
                    return $this->searchOrganization($attributes);
                case 'show_members':
                    return $this->showMembers($attributes);
                case 'show_member':
                    return $this->showMember($attributes);
                case 'add_organization':
                    return $this->addOrganization($attributes);
                case 'remove_organization':
                    return $this->removeOrganization($attributes);
                case 'create_organization':
                    return $this->createOrganization($attributes);
                case 'add_member':
                    return $this->addMember($attributes);
                case 'edit_relationship':
                    return $this->editRelationship($attributes);
                case 'edit_avatar':
                    return $this->editAvatar($attributes);
                case 'delete_member':
                    return $this->deleteMember($attributes);
            }
        } catch (Twig_Error_Runtime $e) {
            return $this->twig->render(
                'error.twig',
                [
                    'exception' => $e,
                    'email' => get_bloginfo('admin_email')
                ]
            );
        }

        return $this->error(__('Could not find this action.'));
    }
}
