<?php
/**
 * ApiTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Api;
use civicrm_api3;
use stdClass;

/**
 * Tests for the Api class.
 */
class ApiTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();
        $this->civicrmApi->foo = new civicrm_api3();
    }

    /**
     * Test the getInstance() function.
     *
     * @return void
     */
    public function testGetInstance()
    {
        Api::deleteInstance();
        $api = Api::getInstance();
        $this->assertInstanceOf(Api::class, $api);
    }

    /**
     * Test the getInstance() function.
     *
     * @return void
     * @expectedException Exception
     * @runInSeparateProcess
     */
    public function testGetInstanceWithoutPath()
    {
        Api::$path = null;
        $api = new Api();
        $this->assertInstanceOf(Api::class, $api);
    }


    /**
     * Test the getSingle() function.
     *
     * @return void
     */
    public function testGetSingle()
    {
        $this->assertInstanceOf(stdClass::class, $this->api->getSingle('foo', ['foo' => 'bar']));
    }

    /**
     * Test the getSingle() function with an API error.
     *
     * @return void
     * @expectedException CivicrmApi\ApiException
     */
    public function testGetSingleWithError()
    {
        $this->api->getSingle('foo', []);
    }

    /**
     * Test the getOptions() function.
     *
     * @return void
     */
    public function testGetOptions()
    {
        $this->assertInternalType('array', $this->api->getOptions('foo', 'goodtype'));
    }

    /**
     * Test the getOptions() function with an API error.
     *
     * @return void
     * @expectedException CivicrmApi\ApiException
     */
    public function testGetOptionsWithError()
    {
        $this->api->getOptions('foo', 'unknowntype');
    }

    /**
     * Test the get() function.
     *
     * @return void
     */
    public function testGet()
    {
        $this->assertInternalType('array', $this->api->get('foo', ['foo' => 'bar']));
    }

    /**
     * Test the get() function with an API error.
     *
     * @return void
     * @expectedException CivicrmApi\ApiException
     */
    public function testGetWithError()
    {
        $this->api->get('foo', ['wrong_constraint']);
    }

    /**
     * Test the getCount() function.
     *
     * @return void
     */
    public function testGetCount()
    {
        $this->civicrmApi->lastResult = 42;
        $this->assertInternalType('int', $this->api->getCount('foo', ['foo' => 'bar']));
    }

    /**
     * Test the getCount() function with an API error.
     *
     * @return void
     * @expectedException CivicrmApi\ApiException
     */
    public function testGetCountWithError()
    {
        $this->api->getCount('foo', ['wrong_constraint']);
    }

    /**
     * Test the create() function.
     *
     * @return void
     */
    public function testCreate()
    {
        $this->civicrmApi->lastResult = (object) ['id' => 42];
        $this->assertInternalType('int', $this->api->create('foo', (object) ['foo' => 'bar']));
    }

    /**
     * Test the create() function with an API error.
     *
     * @return void
     * @expectedException CivicrmApi\ApiException
     */
    public function testCreateWithError()
    {
        $this->api->create('foo', (object) ['wrong_constraint']);
    }

    /**
     * Test the create() function without a returned ID.
     *
     * @return void
     */
    public function testCreateWithoutId()
    {
        $this->civicrmApi->lastResult = null;
        $this->assertNull($this->api->create('foo', (object) ['foo' => 'bar']));
    }

    /**
     * Test the delete() function.
     *
     * @return void
     */
    public function testDelete()
    {
        $this->civicrmApi->lastResult = (object) ['id' => 42];
        $this->assertTrue($this->api->delete('foo', (object) ['foo' => 'bar']));
    }

    /**
     * Test the delete() function with an API error.
     *
     * @return void
     * @expectedException CivicrmApi\ApiException
     */
    public function testDeleteWithError()
    {
        $this->api->delete('foo', (object) ['wrong_constraint']);
    }
}
