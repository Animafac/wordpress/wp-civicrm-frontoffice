<?php
/**
 * File used to load classes and functions required by the tests.
 */

require_once __DIR__.'/../vendor/autoload.php';
require_once __DIR__.'/mock_functions.php';
