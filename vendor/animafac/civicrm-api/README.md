# CiviCRM PHP API

This library allows you to easily use the [CiviCRM PHP API](https://docs.civicrm.org/dev/en/latest/api/usage/#php-classapiphp).

The `civicrm_api3` class is great but it only returns `stdClass` objects.
This library uses full class objects with useful methods and exceptions.

## Usage

### Classes

The following classes are available:

* `Address`
* `Contact`
* `ContactType`
* `Country`
* `Email`
* `EntityTag`
* `LocationType`
* `Note`
* `Phone`
* `Relationship`
* `RelationshipType`
* `StateProvince`
* `Tag`
* `User`
* `Website`

They all correspond to CiviCRM entities.

### Methods

All these classes share a set of common methods.

#### `get`

Returns a property from this object.

```php
$contact = new Contact($id);
$contact->get('display_name');
```

#### `set`

Add a property to this object.

```php
$contact = new Contact($id);
$contact->set('display_name');
```

#### `save`

Saves the object to the database

```php
$contact = new Contact();
$contact->set('display_name');
$contact->save();
```

#### `delete`

Removes the object from the database.

```php
$contact = new Contact($id);
$contact->delete();
```

#### `getAll`

Get all objects from this type.

```php
Contact::getAll();

// Or if you only want a subset.
Contact::getAll(['contact_type' => 'Organization']);
```

#### `getCount`

Get the number of objects from this type.

```php
Contact::getCount();

// Or if you only want a subset.
Contact::getCount(['contact_type' => 'Organization']);
```

#### `getSingle`

Get a single object that matches the specified constraints.

```php
Contact::getSingle(['first_name' => 'foo', 'last_name' => 'bar']);
```

## Setup

You can install this library with Composer:

```bash
composer require animafac/civicrm-api
```

Then you need to include both this library (preferably via the Composer autoloader) and the CiviCRM `class.api.php` file.

```php
use CivicrmApi\Api;
use CivicrmApi\Contact;

require_once __DIR__.'/vendor/autoload.php';
require_once 'path/to/civicrm/api/class.api.php';

Api::$path = '/path/to/civicrm/config/';

$contact = new Contact($id);
echo $contact->get('display_name');
```
