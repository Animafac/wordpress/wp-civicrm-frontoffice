<?php
/**
 * GenderTest class.
 */

namespace CivicrmApi\Test;

use CivicrmApi\Gender;

/**
 * Tests for the Gender class.
 */
class GenderTest extends BaseTest
{

    /**
     * Create mock variables used by tests.
     */
    protected function setUp()
    {
        parent::setUp();

        $this->civicrmApi->values = [
            (object) [
                'value' => 'gender_name',
                'key' => 42
            ]
        ];

        $this->gender = new Gender('gender_name');
    }

    /**
     * Test the constructor with an error.
     *
     * @return void
     * @expectedException Exception
     */
    public function testConstructWithError()
    {
        $gender = new Gender('invalid_gender_name');
    }

    /**
     * Test the getAll() function.
     *
     * @return void
     */
    public function testGetAll()
    {
        foreach (Gender::getAll() as $gender) {
            $this->assertInstanceOf(Gender::class, $gender);
        }
    }
}
